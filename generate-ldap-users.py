#! /usr/bin/env python

import ConfigParser
import optparse
import os
import ldap
import sys


parser = optparse.OptionParser()
parser.add_option('--config', dest='config', default='ldap.ini')
parser.add_option('--output', dest='output_filename',
        default='ocrloader-complete.ini')
parser.add_option('--bindpw', dest='bindpw')
(options, args) = parser.parse_args()
if not options.bindpw:
    parser.error('Missing LDAP bind password')

cfg = ConfigParser.ConfigParser()
cfg.read(options.config)

ldap_conn = ldap.initialize(cfg.get('general', 'ldap_uri'))
ldap_conn.simple_bind_s(cfg.get('general', 'bind_dn'), options.bindpw)

users = []

for entry in ldap_conn.search_s("ou=ouGED,ou=ouPCF,ou=ouUsers,dc=win,dc=info,dc=pcf",
        ldap.SCOPE_SUBTREE, "objectclass=user"):
    if not entry[0]:
        continue

    try:
        username = entry[1]['sAMAccountName'][0]
        mail = entry[1]['mail'][0]
    except KeyError:
        continue
    users.append({'username': username, 'mail': mail})

fd = file(options.output_filename, 'w')
print >> fd, file(cfg.get('general', 'template')).read()

for user in users:
    vars = {'username': user.get('username'),
            'username_lower': user.get('username').lower(),
            'mail': user.get('mail'),
            }
    username = user.get('username')
    vars.update(dict(cfg.items('variables')))
    print >> fd, '''#[ged-gen-%(mail)s]
#default_type = dmsdocument
#default_directory = Members/%(username)s
#user = %(username)s

[ged-%(mail)s]
store_path = /srv/ocr/%(username)s

[/srv/ocr2/%(username)s]
store_path = /srv/ocr/%(username)s

#[ged-test-gen-%(mail)s]
#ged_base_url = %(test_server_url)s
#default_type = dmsdocument
#default_directory = Members/%(username)s
#user = %(username)s

''' % vars

    if not os.path.exists('/srv/ocr/%s' % username):
        os.mkdir('/srv/ocr/%s' % username)

    if not os.path.exists('/srv/ocr2/%s' % username):
        os.mkdir('/srv/ocr2/%s' % username)


fd.close()

